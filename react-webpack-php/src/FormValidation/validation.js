import React from 'react';

const stringValidate = (fieldName, fieldValue) => {
    if (fieldValue.trim() === '') {
        console.log(fieldName, fieldValue);
        const error=`${fieldName} is required`
        return <ErrorField text={error}/>;
    }
}
const ProductTypeValidate = (fieldName, fieldValue) => {
    if (fieldValue === 'None') {
        console.log(fieldName, fieldValue);
        const error=`${fieldName} is required`
        return <ErrorField text={error}/>;
    }
}
const numericValidation = (fieldName, fieldValue) => {
    if (fieldValue === 0 || fieldValue < 0) {
        const error=`${fieldName} is required`
        return <ErrorField text={error}/>;;
    }
}
export const validate = {
    sku: sku => stringValidate("SKU", sku),
    product_type: product_type => ProductTypeValidate("Product Type", product_type),
    name: name => stringValidate("Product Name", name),
    price: price => numericValidation("Price", price),
    size: size => numericValidation("Size", size),
    weight:weight=>numericValidation("Weight", weight),
    height:height=>numericValidation("Height", height),
    width:width=>numericValidation("Width", width),
    length:length=>numericValidation("Length", length),

}
const ErrorField=({text})=>(
    <div className="row mb-4">
      <div className="col-5">
            <div className="alert alert-danger d-flex align-items-center" role="alert">
                <div>
                   {text}
                </div>
            </div>  
        </div>
    </div>
);

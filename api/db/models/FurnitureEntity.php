<?php
class FurnitureEntity{
    private int $product_id;
    private int $width;
    private int $height;
    private int $length;


	/**
	 * 
	 * @return int
	 */
	function getProduct_id(): int {
		return $this->product_id;
	}
	
	/**
	 * 
	 * @param int $product_id 
	 * @return FurnitureEntity
	 */
	function setProduct_id(int $product_id): self {
		$this->product_id = $product_id;
		return $this;
	}
	
	/**
	 * 
	 * @return int
	 */
	function getWidth(): int {
		return $this->width;
	}
	
	/**
	 * 
	 * @param int $width 
	 * @return FurnitureEntity
	 */
	function setWidth(int $width): self {
		$this->width = $width;
		return $this;
	}
	
	/**
	 * 
	 * @return int
	 */
	function getHeight(): int {
		return $this->height;
	}
	
	/**
	 * 
	 * @param int $height 
	 * @return FurnitureEntity
	 */
	function setHeight(int $height): self {
		$this->height = $height;
		return $this;
	}
	
	/**
	 * 
	 * @return int
	 */
	function getLength(): int {
		return $this->length;
	}
	
	/**
	 * 
	 * @param int $length 
	 * @return FurnitureEntity
	 */
	function setLength(int $length): self {
		$this->length = $length;
		return $this;
	}
	/**
	 * @param $product_id int 
	 * @param $width int 
	 * @param $height int 
	 * @param $length int 
	 */
	function __construct( int $width, int $height, int $length) {
	    $this->width = $width;
	    $this->height = $height;
	    $this->length = $length;
	}
}
?>
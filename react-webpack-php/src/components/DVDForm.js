import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ProductFormDescription } from './ProductDescription';

export class DVDForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            size: 0.0
        }
    }
    componentDidMount() {
        this.props.addFormField("size", 0)
    }

    render() {
        console.log("Form Field", this.props)
        return (
            <>
                <div className="row mb-4">
                    <label className="col-1 col-form-label">Size (MB)</label>
                    <div className="col-4">
                        <input type="number" onBlur={this.props.handleBlur} step="512" id="size" name="size"
                            value={this.props.size || ''}
                            onChange={(e) => this.props.handleInputChange(e)} className="form-control" required />
                    </div>
                </div>
                {this.props.touched.size && this.props.errors.size}
                <ProductFormDescription descriptionText={"Please provide size in Megabytes"} />
            </>
        )
    }
}
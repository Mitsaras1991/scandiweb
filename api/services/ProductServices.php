<?php
include_once("IProductServices.php");
include_once("../db/repositories/Product_Repo.php");

class ProductServices implements IProductServices{
    private Product_Repo $productRepo;

	/**
	 *
	 * @param array $ids 
	 *
	 * @return mixed
	 */
	function deleteMany(array $ids) {
		return $this->productRepo->deleteMany($ids);
	}

	
	/**
	 *
	 * @return mixed
	 */
	function fecthAllProduct() {
        return $this->productRepo->fetchAll();
	}
	
	/**
	 *
	 * @return mixed
	 */
	function save() {
	}
	/**
	 * @param $productRepo Product_Repo 
	 */
	function __construct() {
	    $this->productRepo = new Product_Repo();
	}
}
?>
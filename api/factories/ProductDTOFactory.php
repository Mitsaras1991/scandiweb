<?php
include_once("../DTOs/DvdDto.php");
include_once("../DTOs/BookDto.php");
include_once("../DTOs/FurnitureDto.php");

class ProductTypeTablePrimaryKey{
    const DVD=1;
    const Book=2;
    const Furniture=3;
}
class ProductDTOFactory{
    public static function createProduct($requestData){
        try{
            switch($requestData->product_type):
                case "DVD":
                    $dvdDTO=new DvdDto($requestData->sku,
                    $requestData->price,ProductTypeTablePrimaryKey::DVD,$requestData->name);
                    $dvdDTO->setSize($requestData->size);
                    return  $dvdDTO;
                case "Book":
                    $bookDTO=new BookDto($requestData->sku,
                    $requestData->price,ProductTypeTablePrimaryKey::Book,$requestData->name);
                    $bookDTO->setWeight($requestData->weight);
                    return  $bookDTO;
                case "Furniture":
                    $furnitureDTO=new FurnitureDto($requestData->sku,
                    $requestData->price,ProductTypeTablePrimaryKey::Furniture,$requestData->name);
                    $furnitureDTO->setHeight($requestData->height);
                    $furnitureDTO->setLength($requestData->length);
                    $furnitureDTO->setWidth($requestData->width);
                        return  $furnitureDTO;
                default:
                    http_response_code(500);
                    throw new RuntimeException("Item no type");
                endswitch;
        }
        catch(Exception |Error $e){
            http_response_code(500);
            throw new RuntimeException("Item no type");
        }
       

        
 }
}

?>
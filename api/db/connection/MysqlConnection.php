<?php
include_once("DBConnectionInterface.php");
class MysqlConnection implements DBConnection_Interface{
    private $connection;
    //private $conn;
    private  $servername;
    private string $username= "root";
    private string $password= "";
    private string $dbname="store";
	/**
	 *
	 * @return mixed
	 */
	function connect() {
        try {
            $this->connection= new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
            // set the PDO error mode to exception
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_PERSISTENT,true);
            $this->connection->setAttribute(PDO::ATTR_EMULATE_PREPARES,true) ;
      
          //echo "Connected successfully";
          } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
          }
              return $this->connection;
          
	}
	
	/**
	 *
	 * @return mixed
	 */
	function disconnect() {
        $this->connection=null;
	}
	/**
	 * @param $connection mixed 
	 */
	function __construct() {
	    $this->connection =  new PDO("mysql:host=$this->servername;dbname=$this->dbname", $this->username, $this->password);
	}
}
?>
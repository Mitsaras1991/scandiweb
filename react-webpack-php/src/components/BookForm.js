import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ProductFormDescription } from './ProductDescription';
export class BookForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            weight: 0.0
        }
    }
    componentDidMount() {
        this.props.addFormField("weight", 0)
    }

  
    render(){
        return (
        <>
            <div className="row mb-4">
                <label className="col-1 col-form-label">Weight (KG)</label>
                <div className="col-4">
                    <input type="number" id="weight" onBlur={this.props.handleBlur} name="weight" step="0.1"
                        value={this.props.weight} onChange={this.props.handleInputChange}
                        className="form-control" required />
                </div>
            </div>
            {this.props.touched.weight && this.props.errors.weight}
            <ProductFormDescription descriptionText={"Please provide weight in Kilos"} />
        </>
    )
        }
}
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../styles/card_products.scss'
export function GPCIWithChechBox(WrappedComponent) {
    return class extends React.Component {
      componentDidMount() {
        console.log('Current props: ', this.props);
        //console.log('Previous props: ', prevProps);
      }
      render() {
        // Wraps the input component in a container, without mutating it. Good!
        return ( <div  className="col-2  card product-item">
        <input type="checkbox" className="delete-checkbox" onChange={()=>this.props.toggleCheckedProducts(this.props.product_id)} />
        <div className="product-info">
            <p>{this.props.sku}</p>
            <p>{this.props.name}</p>
            <p>{this.props.price}$</p>
            <WrappedComponent {...this.props} />
        </div>
        </div>) ;
      }
    }
  }
<?php
interface ExecutionInterface{
    public function execute(string $sql,$data);
    public function executeRead(string $sql,$data);
    public function executeDelete(string $sql);
}
?>
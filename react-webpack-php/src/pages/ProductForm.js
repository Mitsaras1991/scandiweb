import React from 'react';
import Header from '../common/Header';
import '../styles/form.scss';
//import '../styles/header.scss';

import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import { ProductDescription } from '../components/ProductDescription';
import { DynamicFormProductHandleHOC } from '../components/HOCs/DynamicFormProductHandleHOC';

class ProductForm extends React.Component {
    constructor(props) {
        super(props);
        this.productInput = {
            price: '',
            name: '',
            sku: ''
        }
        this.state = { product_type: 'None' };
    }
    handleProductTypeChange = (event) => {

        this.setState({ [event.target.name]: event.target.value });
    }
    handleInputChange = (event) => {
        const { sku, name, price } = this.productInput;

        this.setState({ [event.target.name]: event.target.value });
    }


    handleFormInput = (name, value) => {
        this.productInput[name] = value;

    }
    render() {
        console.log("Page state", this.state)
        console.log("Page props", this.props)

        return (
            <>
                <Header title="Add Product">
                    <button className="" onClick={() => this.props.saveProduct()} >Save</button>
                    <button type="button" onClick={() => this.props.history.push('/')} id="cancel-product-btn">Cancel</button>
                </Header>
            </>
        )
    }
}


function FormValidate(WrappedComponent) {

    return class extends React.Component {
        constructor(props) {
            super(props)

        }

        render() {
            // Wraps the input component in a container, without mutating it. Good!
            return (
                <>
                    <WrappedComponent validate={validate} />

                </>);
        }
    }
}






export default DynamicFormProductHandleHOC(ProductForm);
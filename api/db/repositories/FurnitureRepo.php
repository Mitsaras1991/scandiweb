<?php
require_once("../db/connection/MysqlConnection.php");
require_once("../db/execution/ExecutionManager.php");
require_once("../db/execution/ExecutionInterface.php");
require_once("FurnitureRepoInterface.php");
class FurnitureRepo implements FurnitureRepoInterface{
    private ExecutionInterface $manager;

	/**
	 * @param $manager ExecutionInterface 
	 */
	function __construct() {
	    $this->manager = new ExecutionManager(new MysqlConnection());
	}
	/**
	 *
	 * @param FurnitureEntity $bookEntity 
	 *
	 * @return mixed
	 */
	function save(FurnitureEntity $furnitureEntity) {
        $insertDVDTable="INSERT INTO furniture (product_id, width,length,height)
        VALUES ( :product_id, :width,:length,:height)";
          $data = [
           "product_id"=>$furnitureEntity->getProduct_id(),
           "width"=>$furnitureEntity->getWidth(),
           "height"=> $furnitureEntity->getHeight(),
           "length"=>$furnitureEntity->getLength()
        ];
      return  $this->manager->execute($insertDVDTable,$data);
	}
}
?>
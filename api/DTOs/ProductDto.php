<?php
class ProductDto{
    public string $sku;
    public float $price;
    public int $product_type;
    public string $name;
	/**
	 */
	
	/**
	 * 
	 * @return string
	 */
	function getSku(): string {
		return $this->sku;
	}
	
	/**
	 * 
	 * @param string $sku 
	 * @return ProductDto
	 */
	function setSku(string $sku): self {
		$this->sku = $sku;
		return $this;
	}
	
	/**
	 * 
	 * @return float
	 */
	function getPrice(): float {
		return $this->price;
	}
	
	/**
	 * 
	 * @param float $price 
	 * @return ProductDto
	 */
	function setPrice(float $price): self {
		$this->price = $price;
		return $this;
	}
	
	/**
	 * 
	 * @return string
	 */
	function getProduct_type(): int {
		return $this->product_type;
	}
	
	/**
	 * 
	 * @param string $product_type 
	 * @return ProductDto
	 */
	function setProduct_type(int $product_type): self {
		$this->product_type = $product_type;
		return $this;
	}
	
	/**
	 * 
	 * @return string
	 */
	function getName(): string {
		return $this->name;
	}
	
	/**
	 * 
	 * @param string $name 
	 * @return ProductDto
	 */
	function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
	/**
	 * @param $sku string 
	 * @param $price float 
	 * @param $product_type string 
	 * @param $name string 
	 */
	function __construct(string $sku, float $price, string $product_type, string $name) {
	    $this->sku = $sku;
	    $this->price = $price;
	    $this->product_type = $product_type;
	    $this->name = $name;
	}
}

?>
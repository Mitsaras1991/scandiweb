<?php
class FurnitureDto extends ProductDto{
    public int $height;
    public int $width;
    public int $length;
    function __construct(string $sku, float $price, int $product_type, string $name) {
        parent::__construct( $sku,  $price,  $product_type,  $name);
	}
	/**
	 * 
	 * @param int $height 
	 * @return FurnitureDto
	 */
	function setHeight(int $height): self {
		$this->height = $height;
		return $this;
	}
	
	/**
	 * 
	 * @param int $width 
	 * @return FurnitureDto
	 */
	function setWidth(int $width): self {
		$this->width = $width;
		return $this;
	}
	
	/**
	 * 
	 * @param int $length 
	 * @return FurnitureDto
	 */
	function setLength(int $length): self {
		$this->length = $length;
		return $this;
	}
}

?>
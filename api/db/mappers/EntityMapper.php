<?php
class EntityMapper{
    private $dto;
    private $entity;

	function __construct($dto,$entity) {
        $this->dto=$dto;
        $this->entity=new ReflectionClass($entity);
	}
    private function getEntityFields(array $properties){
        $fields = [];
		foreach ($properties as $prop) {
			array_push($fields, $prop->getName());
		}
		return $fields;
    }
    public function convertToEntity(){
     //$DtoProperties = $this->dto->getProperties();
     $EntityFields = $this->getEntityFields($this->entity->getProperties());
     $EntityProperties=[];
     foreach ($EntityFields as $prop) {
         if(property_exists($this->dto,$prop)){
            $EntityProperties[$prop]= $this->dto->$prop;
        }
        
       

    }
    return $this->entity->newInstanceArgs($EntityProperties);
}
	/**
	 * 
	 * @return mixed
	 */
	function getEntity() {
		return $this->entity;
	}
	
	/**
	 * 
	 * @param mixed $entity 
	 * @return EntityMapper
	 */
	function setEntity($entity): self {
		$this->entity = new ReflectionClass($entity);
		return $this;
	}
}
?>
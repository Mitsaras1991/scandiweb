<?php
include_once("ProductDto.php");
class DvdDto extends ProductDto{
    public int $size;
	/**
	 * 
	 * @return int
	 */
	function getSize(): int {
		return $this->size;
	}
	
	/**
	 * 
	 * @param int $size 
	 * @return DvdDto
	 */
	function setSize(int $size): self {
		$this->size = $size;
		return $this;
    }
	/**
	 */
	function __construct(string $sku, float $price, int $product_type, string $name) {
        parent::__construct( $sku,  $price,  $product_type,  $name);
	}
}
?>
<?php
include_once("ExecutionInterface.php");
//inclu_include_oncede("../connection/MysqlConnection.php");
include_once("../db/connection/DBConnectionInterface.php");
include_once("../db/connection/MysqlConnection.php");


class ExecutionManager implements ExecutionInterface{
    
    private  $db;
	/**
	 *
	 * @param string $sql 
	 * @param mixed $data 
	 *
	 * @return mixed
	 */
    public function executeRead(string $sql,$data){
        try{
            $conn=$this->db->connect();
            
            $st=$conn->prepare($sql);
            try{
                $conn->beginTransaction();              
                $st->execute($data);
                $result = $st->fetchAll(PDO::FETCH_ASSOC);
                $conn->commit();
                return $result;  
                
            }
            catch(PDOException $e){

                $conn->rollback();
                //http_response_code(500);
                return $e;
            }
            
        }
        catch(PDOException $e){
            $conn->rollback();
           // http_response_code(500);

            return $e;
        }finally{
            $this->db->disconnect();
        }

    }
	function executeDelete(string $sql) {
        try{
            $conn=$this->db->connect();
            
            $st=$conn->prepare($sql);
            try{
                $conn->beginTransaction();              
                $st->execute();
                //$id=$conn->lastInsertId();
                $conn->commit();
                return "Successfully deleted";  
                
            }
            catch(PDOException $e){

                $conn->rollback();
              //  http_response_code(500);
                return $e;
            }
            
        }
        catch(PDOException $e){
            $conn->rollback();
            //http_response_code(500);

            return $e;
        }finally{
            $this->db->disconnect();
        }
	}
	function execute(string $sql, $data) {
        try{
            $conn=$this->db->connect();
            
            $st=$conn->prepare($sql);
            try{
                $conn->beginTransaction();              
                $st->execute($data);
                $id=$conn->lastInsertId();
                $conn->commit();
                return $id;  
                
            }
            catch(PDOException $e){

                $conn->rollback();
               // http_response_code(500);
                return $e;
            }
            
        }
        catch(PDOException $e){
            $conn->rollback();
            //http_response_code(500);

            return $e;
        }finally{
            $this->db->disconnect();
        }
	}
	/**
	 * @param $db DBConnectionInterface 
	 */
	function __construct(DBConnection_Interface $db) {
	    $this->db = $db;
	}
}

?>
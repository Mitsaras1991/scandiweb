const path=require('path')
module.exports={
    entry: './src/index.js',
    output: {
      path: path.join(__dirname, 'public'),
      filename: 'bundle.js'
    },
    module: {
      rules:[
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
          },
          {
               test: /\.css$/,
              use: ['style-loader', 'css-loader']
             },
          {
            test: /\.scss$/,
            use: ['style-loader', 'css-loader', 'sass-loader']
          }
          
      ]
    },
    devtool: 'eval-cheap-module-source-map',
    devServer: {
      historyApiFallback: true,
      static: {
        directory: path.join(__dirname, 'public'),
      },
      compress: true,
      port: 9000,
      proxy: {
        '/api/': {
          target: 'http://localhost',
          secure: false,
          changeOrigin: true
        }
      }
   
    } 
/*     module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        // {
        //   test: /\.css$/,
        //   use: ['style-loader', 'css-loader']
        // }
        {
          test: /\.scss$/,
          use: ['style-loader', 'css-loader', 'sass-loader']
        }
      ]
    },
    devtool: 'cheap-module-eval-source-map',
    devServer: {
      contentBase: path.join(__dirname, 'public')
    } */
}
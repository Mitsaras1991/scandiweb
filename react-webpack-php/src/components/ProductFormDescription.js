import React from 'react';
import { ProductTypeFormFactory } from './factories/ProductTypeFormFactory';
export const ProductTypeForm = (props) => {
    return ProductTypeFormFactory.createForm(props)
}
<?php
include_once("BookRepoInterface.php");
require_once("../db/connection/MysqlConnection.php");
require_once("../db/execution/ExecutionManager.php");
require_once("../db/execution/ExecutionInterface.php");
class BookRepo implements BookRepoInterface{
    private ExecutionInterface $manager;

	/**   

	 *
	 * @param DvdEntity $dvdEntity 
	 *
	 * @return mixed
	 */
	function save(BookEntity $bookEntity) {
        $insertBooTable="INSERT INTO book (	product_id, weight)
        VALUES ( :product_id, :weight)";
          $data = [
            //'product_id' =>$id,
            
           "product_id"=>$bookEntity->getProduct_id(),
           "weight"=>$bookEntity->getWeight()

        ];
        return $this->manager->execute($insertBooTable,$data);


	}
	/**
	 */
	function __construct() {
        $this->manager = new ExecutionManager(new MysqlConnection());

	}
}
?>
import './styles/style.scss';
import React from 'react';
import {Switch,Route,BrowserRouter as Router}  from 'react-router-dom'
import ProductListView from './pages/ProductListView';
import ProductForm from './pages/ProductForm';
import { Footer } from './common/Footer';

//import axios from 'axios'
export default class App extends React.Component{

  constructor(){
      super();

      
  }

  

  render(){
      return(
          <Router>
              <Switch>
                <Route exact path="/" component={ProductListView}/>
                <Route path="/add-product" component={ProductForm}/>
              </Switch>
              <Footer/>
          </Router>
      );
  }
}
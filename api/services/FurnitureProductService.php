<?php
include_once("../db/models/ProductTable.php");
include_once("../db/repositories/Product_Repo.php");
include_once("../db/models/FurnitureEntity.php");
include_once("../db/repositories/FurnitureRepo.php");
include_once("IFurnitureService.php");
include_once("../db/mappers/EntityMapper.php");
include_once("../DTOs/FurnitureDto.php");

class FurnitureProductService implements IFurnitureService{
    private Product_Repo $productRepo;
    private FurnitureRepo $furnitureRepo;
    public function save(FurnitureDto $furnitureDto){
        $mapper=new EntityMapper($furnitureDto,ProductTable::class);
        $product=$mapper->convertToEntity();
        $i=$this->productRepo->save($product);
        $mapper->setEntity(FurnitureEntity::class);
        $furnitureEntity=$mapper->convertToEntity();
        $furnitureEntity->setProduct_id($i);
        $this->furnitureRepo->save($furnitureEntity);
        return $furnitureEntity;
    }

	/**
	 */
	function __construct() {
        $this->productRepo=new Product_Repo();
        $this->furnitureRepo=new FurnitureRepo();
	}
}
?>
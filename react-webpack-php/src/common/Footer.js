import React from 'react';
import '../styles/footer.scss'
export const Footer=()=>{
    return(
        <div className="footer">
            <hr/>
            <div className="footer-content">
                Scandiweb Test assignment 
            </div>
        </div>
    )
}
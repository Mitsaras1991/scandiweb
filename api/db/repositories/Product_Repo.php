<?php
//class Dvd_Model
include_once("../db/models/ProductTable.php");
require_once("../db/connection/MysqlConnection.php");
require_once("../db/execution/ExecutionManager.php");
require_once("../db/execution/ExecutionInterface.php");
include_once("ProductRepoInterface.php");
class Product_Repo implements Product_RepoInterface{
    private ExecutionInterface $manager;

    //private DVD_Model $dvd;
	/**
	 *
	 * @return mixed
	 */
	function save(ProductTable $product) {
        $insertDVDTable="INSERT INTO product (	sku, price, product_type_id	,name)
        VALUES (:SKU, :Price,:ProductType,:Name)";
          $data = [
            //'product_id' =>$id,
            'SKU' => $product->getSKU(),
            'Price'=> $product->getPrice(),
            'ProductType' => $product->getProduct_type(),
            'Name' => $product->getName(),

        ];
        return  $this->manager->execute($insertDVDTable,$data);

       
	}
	/**
	 * @param $dvd DVD_Model 
	 * @param $db DB 
	 */
	function __construct() {
	    
	    $this->manager = new ExecutionManager(new MysqlConnection());
    
	}
	/**
	 *
	 * @return mixed
	 */ 
	function fetchAll() {
       $sql= "SELECT *,product.product_id
	FROM product
	LEFT  JOIN product_type ON product.	product_type_id   = product_type.id	 
	LEFT  JOIN dvd ON product.product_id = dvd.product_id
	LEFT  JOIN book ON product.product_id = book.product_id
	LEFT  JOIN furniture ON product.product_id = furniture.product_id
	ORDER BY product.product_id";
		return           $this->manager->executeRead($sql,null);

	}
	
	
	
	/**
	 *
	 * @param array $ids 
	 *
	 * @return mixed
	 */
	function deleteMany(array $ids) {
        $sql= "DELETE 
        FROM product
        WHERE product.product_id IN (" .implode(',', $ids).")";
    return         $this->manager->executeDelete($sql);

	}
}
?>
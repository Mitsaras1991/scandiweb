<?php
class BookEntity{
    private int $product_id;
    private float $weight;
    
	/**
	 * 
	 * @return int
	 */
	function getProduct_id(): int {
		return $this->product_id;
	}
	
	/**
	 * 
	 * @param int $product_id 
	 * @return BookEntity
	 */
	function setProduct_id(int $product_id): self {
		$this->product_id = $product_id;
		return $this;
	}
	
	/**
	 * 
	 * @return int
	 */
	function getWeight(): float {
		return $this->weight;
	}
	
	/**
	 * 
	 * @param int $weight 
	 * @return BookEntity
	 */
	function setWeight(float $weight): self {
		$this->weight = $weight;
		return $this;
	}
	/**
	 */
	function __construct(float $weight) {
        $this->weight=$weight;
	}
}
?>
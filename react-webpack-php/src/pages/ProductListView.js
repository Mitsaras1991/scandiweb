import React from 'react';
import { NavLink } from 'react-router-dom';
import Header from '../common/Header';
import 'bootstrap/dist/css/bootstrap.min.css';
import axios from 'axios';
import ProductCardsList from '../components/ProductCardsList';
import '../styles/products_list.scss'

class ProductListView extends React.Component{
    constructor(props){
        super(props);
        this.state={
            products:[],
            selectedProducts:new Set()
        }
        this.toggleCheckedProducts = this.toggleCheckedProducts.bind(this);
    }
     toggleCheckedProducts(id){
         const {selectedProducts}=this.state
        console.log(this.state.selectedProducts.has(id));
        if(selectedProducts.has(id)){
            selectedProducts.delete(id)
        }else{
            selectedProducts.add(id)
        }
    }
    deleteProducts=()=>{
        const {selectedProducts}=this.state
       const pr={
            ids:[...selectedProducts]
        }
     
        axios.post("api/services/delete_products.php",{pr}).then((response)=>{
            console.log(response);
            window.location.reload();
        }).catch(error=>{
            console.log(error);
          })

       
    }
     componentDidMount(){
        console.log("Api")
          axios.get("api/services/search.php")
          .then( (response)=>{
            console.log(response);
            this.setState({
                products: response.data,
                selectedProducts:new Set()
            })
            
            }).catch(error=>{
            console.log(error);
          })
        }
    render(){
        console.log(this.state.selectedProducts)
        return(<>
            <Header title="Product List">
                <button  onClick={() => this.props.history.push('/add-product') } >ADD</button>
                <button type="button" onClick={()=>this.deleteProducts()}  id="delete-product-btn">MASS DELETE</button> 
            </Header>   
            <div className="products-container row container-fluid">
                <ProductCardsList {...this.props} productlist={this.state.products} toggleCheckedProducts={this.toggleCheckedProducts}/>    
            </div>

        </>)
    
}
}

export default ProductListView;
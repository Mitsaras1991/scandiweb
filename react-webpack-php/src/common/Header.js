import React from 'react';
import '../styles/header.scss'
import 'bootstrap/dist/css/bootstrap.min.css';

class Header extends React.Component{
    constructor(props){
        super(props)
    }
    render(){
        return(
            <div className="header">
            <div className="header-container">
                <div className="header-title  font-weight-bold fs-3">
                    <p className="font-weight-bold  fw-bold fst-italic ">{this.props.title}</p>
                </div>
                <div id="header-children">
                    {this.props.children}
                </div>
                
            </div>
            <hr />
            </div>
        )
    }
}
export default Header;
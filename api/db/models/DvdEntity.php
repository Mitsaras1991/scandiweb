<?php
class DvdEntity{
    private int $product_id;
    private int $size;

	/**
	 * 
	 * @return int
	 */
	function getProduct_id(): int {
		return $this->product_id;
	}
	
	/**
	 * 
	 * @param int $product_id 
	 * @return DvdEntity
	 */
	function setProduct_id(int $product_id): self {
		$this->product_id = $product_id;
		return $this;
	}
	
	/**
	 * 
	 * @return int
	 */
	function getSize(): int {
		return $this->size;
	}
	
	/**
	 * 
	 * @param int $size 
	 * @return DvdEntity
	 */
	function setSize(int $size): self {
		$this->size = $size;
		return $this;
	}
	/**
	 * @param $product_id int 
	 * @param $size int 
	 */
	function __construct( int $size) {
	   // $this->product_id = $product_id;
	    $this->size = $size;
	}
}
?>
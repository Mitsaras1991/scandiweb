import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';

export const ProductFormDescription=({descriptionText})=>(
    <div className="row mb-4">
    <div className="col-4">
        <p>{descriptionText}</p>  
    </div>
  </div>
)
import React from 'react';
import { BookProductCard } from "../BookProductCard";
import { DvdProductCard } from "../DvdProductCard";
import { FurnitureProductCard } from "../FurnitureProductCard";
import { GPCIWithChechBox } from '../HOCs/CardHOC';

export class ProductCardFactory{
    static createProductCard(props,key){
        console.log(props);
        console.log(key);

        switch(props.product_type){
            case("DVD"):
            const DvdAttributesCard=GPCIWithChechBox(DvdProductCard)
            return <DvdAttributesCard key={key} {...props}/>
            case("Furniture"):
            const FurnitureAttributesCard=GPCIWithChechBox(FurnitureProductCard)

            return <FurnitureAttributesCard key={key} {...props}/>
            case("Book"):
            const BookAttributesCard=GPCIWithChechBox(BookProductCard)

            return <BookAttributesCard key={key} {...props}/>
            default:
                return null;
        }
            
    }
}
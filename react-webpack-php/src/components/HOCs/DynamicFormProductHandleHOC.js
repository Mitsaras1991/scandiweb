import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ProductTypeFormFactory } from '../factories/ProductTypeFormFactory';
import { ProductTypeForm } from '../ProductTypeForm';
import { validate } from '../../FormValidation/validation';
export function DynamicFormProductHandleHOC(WrappedComponent) {
    return class extends React.Component {
        constructor(props) {
            super(props)
                this.state = {
                    touched:{},
                    errors:{},
                    formfields:{
                        product_type: 'None',
                        sku: '',
                        name: '',
                         price: 0
                        }
                }
        }
        componentDidMount() {
            console.log('Current props: ', this.props);
            //console.log('Previous props: ', prevProps);
        }
        handleTypeChange = (event) => { 
            const fields = ["sku", "name", "price"]
            const filtered = Object.keys(this.state.formfields)
                .filter(key => fields.includes(key))
                .reduce((obj, key) => {
                    obj[key] = this.state.formfields[key];
                    return obj;
                }, {});
            console.log("REMOVE KEY", filtered);      
            let p={...filtered} 
            const  {formfields,product_type,touched}=this.state
            this.setState({ touched:{...touched,[event.target.name]:true},
                formfields:{...filtered,[event.target.name]: event.target.value}});
        }
        handleInputChange = (event) => {
            console.log(event.target.name)
            const  {formfields,product_type,touched}=this.state
            const temp=formfields
           temp[event.target.name]= event.target.value
            this.setState({ touched:{...touched,[event.target.name]:true},formfields:{...temp,...formfields}});
            //this.props.handleProductInputFields([event.target.name], event.target.value)
        }
         handleBlur = evt => {
            const { name, value } = evt.target;
        
            // remove whatever error was there previously
            const { [name]: removedError, ...rest } = this.state.errors;
            console.log("Error",rest) 
            // check for a new error
            const error = validate[name](value);
        
            // // validate the field if the value has been touched
            this.setState({
              errors:{...rest,
              ...(error && { [name]: this.state.touched[name] && error })}
            });
          }
           addFormField = (key, value) => {
            console.log('ADD FORMFIELD: ', this.state);

           const  {formfields,product_type}=this.state
           const temp=formfields
           temp[key]=value
            this.setState({ formfields:{...formfields,[key]:value }})
        }

        saveProduct = () => {
            const errors = {}
            const touched = {}
            const formValidation = Object.keys(this.state.formfields).reduce(
                (acc, key) => {
                  const newError = validate[key](this.state.formfields[key]);
                  const newTouched = { [key]: true };
                  return {
                    errors: {
                      ...acc.errors,
                      ...(newError && { [key]: newError }),
                    },
                    touched: {
                      ...acc.touched,
                      ...newTouched,
                    },
                  };
                },
                {
                  errors: { ...errors },
                  touched: { ...touched },
                },
              )
            
                Object.keys(this.state.formfields).forEach((key) => {
                    console.log(`${key} : ${this.state.formfields[key]}`);
                     touched[key]= true 

                    const er=  validate[key](this.state.formfields[key])

                    errors[key]=er
                     console.log(`${errors[key]}`);
                    //return error

                })
                console.log(formValidation)

            //console.log(Object.values(touched).length);
            //console.log(Object.values(this.state.formfields).length)
            //console.log( Object.values(touched).every(t => t === true))

            this.setState({ ...formValidation })
           
            if (
                !Object.values(formValidation.errors).length && // errors object is empty
                Object.values(formValidation.touched).length ===
                Object.values(this.state.formfields).length && // all fields were touched
                Object.values(formValidation.touched).every(t => t === true) // every touched field is true
              ) {
                const productDTO={
                    ...this.state.formfields,
                }
               // alert(JSON.stringify(this.state.formfields, null, 2));
                axios.post("api/services/create_product.php",{productDTO})
                .then(response=>{
                  console.log(response)
                  this.props.history.push("/")
                }).catch(e=>console.log(e))
              }
        }

        render() {
            console.log('Current state: ', this.props);
            const { handleProductTypeChange, handleProductInputFields, ...rest } = this.props

            // Wraps the input component in a container, without mutating it. Good!
            return (
                <>
                    <WrappedComponent saveProduct={this.saveProduct} {...this.props}/>
                    <div className="container-fluid ">
                    <form id="product_form" >
                        <div className="col mb-3">
                            <div className="row mb-4">
                                <label className="col-1 col-form-label">SKU</label>
                                <div className="col-4">
                                    <input type="text" id="sku" name="sku" onBlur={this.handleBlur} onChange={this.handleInputChange} className="form-control"
                                        value={this.state.formfields.sku} required />
                                </div>
                            </div>
                            {this.state.touched.sku && this.state.errors.sku}
                                                                    
                            <div className="row mb-4">
                                <label className="col-1 col-form-label">Name</label>
                                <div className="col-4">
                                    <input type="text" onBlur={this.handleBlur} 
                                    name="name" className="form-control" id="name"
                                        onChange={this.handleInputChange} className="form-control"
                                        value={this.state.formfields.name || ''} required />
                                </div>
                            </div>
                                {this.state.touched.name && this.state.errors.name}
                            <div className="row mb-4">
                                <label className="col-1 col-form-label">Price($)</label>
                                <div className="col-4">
                                    <input type="number" name="price" onBlur={this.handleBlur} step="0.01" className="form-control" id="price"
                                        onChange={this.handleInputChange} className="form-control"
                                        value={this.state.formfields.price || ''} required />
                                </div>
                            </div>
                            {this.state.touched.price && this.state.errors.price}
                            <div className="row mb-4">
                                <label className="col-2 col-form-label">TypeSwitcher:</label>
                                <div className="col-3">
                                    <select name="product_type" id="productType" onBlur={this.handleBlur} required value={this.state.formfields.product_type} onChange={this.handleTypeChange}>
                                        <option value="None" disabled>Type Switcher</option>
                                        <option value="DVD">DVD</option>
                                        <option value="Book">Book</option>
                                        <option value="Furniture">Furniture</option>
                                    </select>
                                </div>
                            </div>
                            {this.state.touched.product_type && this.state.errors.product_type}
                        </div>
                        <ProductTypeForm
                            handleInputChange={this.handleInputChange}
                            addFormField={this.addFormField}
                            handleBlur={this.handleBlur}
                            {...this.props}
                            {...this.state}
                            {...this.state.formfields}

                        />
                    </form>
                    </div>

                </>);
        }
    }
}
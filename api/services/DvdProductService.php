<?php
include_once("../db/models/ProductTable.php");
include_once("../db/repositories/Product_Repo.php");
include_once("../db/models/DvdEntity.php");
include_once("../db/repositories/DvdRepo.php");
include_once("IDvdService.php");
include_once("../db/mappers/EntityMapper.php");
include_once("../DTOs/DvdDto.php");

    class DvdProductService implements IDvdService{
        private Product_Repo $productRepo;
        private DvdRepo $dvdRepo;
        
    	/**
	 */
    public function save(DvdDto $dvdDto){
        //$pr=new ProductTable();
        $mapper=new EntityMapper($dvdDto,ProductTable::class);
        $product=$mapper->convertToEntity();
        $i=$this->productRepo->save($product);
        $mapper->setEntity(DvdEntity::class);
        $dvdEntity=$mapper->convertToEntity();
        $dvdEntity->setProduct_id($i);
        $this->dvdRepo->save($dvdEntity);
        return $dvdEntity;
        //ProductDto =ProductConveter::toEntity($dvdDto,ProductTable::class);
    }
	function __construct() {
        $this->productRepo=new Product_Repo();
        $this->dvdRepo=new DvdRepo();
	}
}

?>
<?php
//class Dvd_Model
require_once("DvdRepoInterface.php");
require_once("../db/connection/MysqlConnection.php");
require_once("../db/execution/ExecutionManager.php");
require_once("../db/execution/ExecutionInterface.php");
//include_once("../execution/ExecutionInterface.php");
//require_once("ExecutionManager.php");

class DvdRepo implements DvdRepoInterface{
    
 
 
    //private DVD_Model $dvd;
    private ExecutionInterface $manager;
	/**
	 *
	 * @return mixed
	 */
	function save(DvdEntity $dvdEntity) {
        $insertDVDTable="INSERT INTO dvd (	product_id, size)
            VALUES ( :product_id, :size)";
              $data = [
               "product_id"=>$dvdEntity->getProduct_id(),
               "size"=>$dvdEntity->getSize()
            ];
          return  $this->manager->execute($insertDVDTable,$data);
       
	}
	/**
	 * @param $dvd DVD_Model 
	 * @param $db DB 
	 */
	function __construct() {
	    //$this->dvd = $dvd;
	    $this->manager = new ExecutionManager(new MysqlConnection());
	}
}
?>
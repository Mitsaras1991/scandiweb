<?php
include_once("../db/models/ProductTable.php");
include_once("../db/repositories/Product_Repo.php");
include_once("../db/repositories/BookRepo.php");
include_once("../db/mappers/EntityMapper.php");
include_once("../db/models/BookEntity.php");
include_once("IBookService.php");

class BookProductService implements IBookService{
    private Product_Repo $productRepo;
    private BookRepo $bookRepo;

	/**
	 *
	 * @param BookDto $bookDto 
	 *
	 * @return mixed
	 */
	function save(BookDto $bookDto) {
        $mapper=new EntityMapper($bookDto,ProductTable::class);
        $product=$mapper->convertToEntity();
        $i=$this->productRepo->save($product);
        $mapper->setEntity(BookEntity::class);
        $bookEntity=$mapper->convertToEntity();
        $bookEntity->setProduct_id($i);
        $this->bookRepo->save($bookEntity);
        return $bookEntity;
	}
	/**
	 * @param $productRepo Product_Repo 
	 * @param $bookRepo BookRepo 
	 */
	function __construct() {
	    $this->productRepo = new Product_Repo();
	    $this->bookRepo = new BookRepo();
	}
}

?>
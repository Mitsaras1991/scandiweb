import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/card_products.scss'
import { ProductCardFactory } from './factories/ProductCardFactory';

export default class ProductCardsList extends React.Component{
    constructor(props){
        super(props);
        
    }
    render(){
        const { productlist ,toggleCheckedProducts}=this.props;
        console.log(toggleCheckedProducts);
        return(
           <>
            {productlist && productlist.map((product,key)=>{
                let props={
                    ...product,
                    toggleCheckedProducts
                    
                }
                return (ProductCardFactory.createProductCard(props,key))
                })}
            </>
            
        )
        
    }
}






import React from 'react';
import { BookForm } from '../BookForm';
import { DefaultForm } from '../DefaultForm';
import { DVDForm } from '../DVDForm';
import { FurnitureForm } from '../FurnitureForm';
export class ProductTypeFormFactory {
    static createForm(props) {
        console.log(props);

        switch (props.product_type) {
            case ("DVD"):
                return <DVDForm {...props} />
            case ("Book"):
                return <BookForm {...props} />
            case ("Furniture"):
                return <FurnitureForm {...props} />
            default:
                return <DefaultForm {...props} />;
        }

    }
}
<?php
include_once("ProductDto.php");
class BookDto extends ProductDto{
    public float $weight;
    function __construct(string $sku, float $price, int $product_type, string $name) {
        parent::__construct( $sku,  $price,  $product_type,  $name);
	}
	/**
	 * 
	 * @return int
	 */
	function getWeight(): float {
		return $this->weight;
	}
	
	/**
	 * 
	 * @param int $weight 
	 * @return BookDto
	 */
	function setWeight(float $weight): self {
		$this->weight = $weight;
		return $this;
	}
}

?>
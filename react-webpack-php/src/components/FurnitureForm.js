import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { ProductFormDescription } from './ProductDescription';

export class FurnitureForm extends React.Component  {
    constructor(props) {
        super(props)
       
    }
    componentDidMount() {
        this.props.addFormField("width", 0)
        this.props.addFormField("height", 0)
        this.props.addFormField("length", 0)
    }
   render(){
    return (
        <>
            <div className="row mb-4">
                <label className="col-1 col-form-label">Height (CM)</label>
                <div className="col-4">
                    <input type="number" id="height" onBlur={this.props.handleBlur} name="height" min="1" step="1"
                        value={this.props.height || ''} onChange={this.props.handleInputChange} 
                        className="form-control" required />
                </div>
            </div>
            {this.props.touched.height && this.props.errors.height}
            <div className="row mb-4">
                <label className="col-1 col-form-label">Width (CM)</label>
                <div className="col-4">
                    <input type="number" onBlur={this.props.handleBlur} id="width" name="width" min="1" step="1"
                        value={this.props.width || ''}
                        onChange={this.props.handleInputChange}
                        className="form-control" required />
                </div>
            </div>
           {this.props.touched.width && this.props.errors.width}
            <div className="row mb-4">
                <label className="col-1 col-form-label">Length (CM)</label>
                <div className="col-4">
                    <input type="number" id="length"onBlur={this.props.handleBlur}  name="length" min="1" step="1"
                        value={this.props.length || ''} onChange={this.props.handleInputChange}
                        className="form-control" required />
                </div>
            </div>
            {this.props.touched.length && this.props.errors.length}
            <ProductFormDescription descriptionText={"Please provide dimension in HxWxL format"} />
        </>

    )
   } 
}
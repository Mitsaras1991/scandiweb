<?php
include_once("../services/BookProductService.php");
include_once("../services/DvdProductService.php");
include_once("../services/FurnitureProductService.php");

class ServiceFactory{
    public static function createService($requestData){
        
            switch($requestData->product_type):
                case "DVD":
    
                    return  new DvdProductService();
                case "Book":
    
                    return  new BookProductService();
                case "Furniture":
    
                        return  new FurnitureProductService();
                default:
                    throw new RuntimeException("Item no type");
                endswitch;
        
        
    }

}
?>